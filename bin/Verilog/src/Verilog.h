//
// Created by a404m on 3/20/23.
//

#ifndef VERILOG_VERILOG_H
#define VERILOG_VERILOG_H

#include <Godot.hpp>
#include <Node.hpp>
#include "verilog.h"
#include <string>
//#include <runner/Runner.h>

namespace godot {
using namespace verilog;
    class Verilog : public godot::Node {
        GODOT_CLASS(Verilog,godot::Node)
    public:
        Verilog();
        ~Verilog();

        static void _register_methods();
        void _init();
        void _exit_tree();

        godot::Variant run(godot::Variant moduleName,godot::Array inputs);

        godot::Variant compile(godot::Variant code);
        void print(godot::Variant var);
        godot::Variant moduleNames();
        void clearModules();

    private:
        static std::string toCXXStr(godot::String str);
        //static godot::String toGodotStr(const std::string &str);
        static State toState(int val);


        static Module *lastModule;
    };

} // godot

#endif //VERILOG_VERILOG_H
