//
// Created by a404m on 3/20/23.
//

#include "Verilog.h"

namespace godot {
    Module *Verilog::lastModule = nullptr;

    Verilog::Verilog(){
		//empty
	}
	
	Verilog::~Verilog(){
		//empty
	}
	
    void Verilog::_register_methods() {
        godot::register_method("run",&Verilog::run);
        godot::register_method("compile",&Verilog::compile);
        godot::register_method("moduleNames",&Verilog::moduleNames);
        godot::register_method("clearModules",&Verilog::clearModules);
        godot::register_method("_exit_tree",&Verilog::_exit_tree);
    }

    void Verilog::_init() {
        //Node::_init();
        //micoSeconds = 1000;
        delete lastModule;
        lastModule = nullptr;
    }

    void Verilog::_exit_tree(){
        delete lastModule;
        lastModule = nullptr;
    }

    godot::Variant Verilog::run(godot::Variant moduleName, godot::Array inputs) {
    	try{
			std::vector<std::shared_ptr<Variable>> args;
			Module *module;
			auto mName = toCXXStr(moduleName);
			if(lastModule != nullptr && lastModule->name == mName) {
                module = lastModule;
            }else{
                module = Module::find(mName);
                if(module == nullptr){
                    lastModule = nullptr;
                    throw std::runtime_error("no module found "+ toCXXStr(moduleName));
                }
                module = module->pointerInstance();
                delete lastModule;
                lastModule = module;
            }
			godot::Array outputs;
			outputs.resize(module->params.size()-inputs.size());
			for(int i = 0;i < outputs.size();++i){
			    args.emplace_back(std::make_shared<Variable>("",State::UNDEF));
			}
			for(int i = 0;i < inputs.size();i++){
			    args.push_back(std::make_shared<Variable>("i"+std::to_string(i),toState(inputs[i])));
			}
            module->run(args);
			for(int i = 0;i < outputs.size();i++) {
                outputs[i] = (int)args[i]->state;
            }
			return outputs;
        }catch(const std::exception &e){
        	Module::modulesCalled.clear();
            print((std::string("C++-Error:")+e.what()).c_str());
        	return e.what();
        }
    }

    godot::Variant Verilog::compile(godot::Variant code) {
    	try{
            delete lastModule;
            lastModule = nullptr;
        	Runner::compile(toCXXStr(code));
            return (int)Error::OK;
        }catch(const std::exception &e){
            print((std::string("C++-Error:")+e.what()).c_str());
            return e.what();
        }
    }
    
    void Verilog::print(godot::Variant var){
    	godot::Godot::print(var);
    }

    godot::Variant Verilog::moduleNames() {
        godot::Array result;
        for(const auto &module : Module::modules){
            result.push_back(module.name.c_str());
        }
    	result.push_back("nand");
    	result.push_back("buf");
    	result.push_back("bufif1");
        return result;
    }

    void Verilog::clearModules(){
        Module::modules.clear();
        Module::modulesCalled.clear();
        delete lastModule;
        lastModule = nullptr;
    }

    std::string Verilog::toCXXStr(godot::String str) {
        std::string stdStr(str.utf8().get_data(),str.utf8().length());
        return stdStr;
    }

    //godot::String Verilog::toGodotStr(const std::string &str) {
    //    return {str.c_str()};
    //}

    State Verilog::toState(int val) {
        switch(val){
            case (int)State::UNDEF:
                return State::UNDEF;
            case (int)State::LOW:
                return State::LOW;
            case (int)State::HIGH:
                return State::HIGH;
            default:
                throw std::runtime_error("unexpected state "+std::to_string(val));
        }
    }
} // godot
