#ifndef VERILOG_HPP
#define VERILOG_HPP 1

#include <cstdint>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <memory>
#include <set>
#include <stdexcept>
#include <exception>

namespace verilog {
    enum class State : int8_t {
        UNDEF = -1,
        LOW = 0,
        HIGH = 1
    };
    class Lexer {
    public:
        using Lexed = std::vector<std::string>;
        static std::vector<std::string> lex(const std::string &code) {
            std::vector<std::string> res;
            bool isID = false;
            std::string str;
            for(auto c : code){
                if(isOperator(c)){
                    if(!str.empty()){
                        using std::swap;
                        swap(res.emplace_back(),str);
                    }
                    isID = false;
                }else if(isIdentifier(c)){
                    if(!isID && !str.empty()){
                        using std::swap;
                        swap(res.emplace_back(),str);
                    }
                    isID = true;
                }else{
                    if(!str.empty()){
                        using std::swap;
                        swap(res.emplace_back(), str);
                    }
                    continue;
                }
                str += c;
            }

            if(!str.empty())
                res.push_back(str);

            return res;
        }

        static bool isOperator(char c) {
            switch(c){
                case ';':
                case ',':
                case ')':
                case '(':
                    return true;
                default:
                    return false;
            }
        }

        static bool isIdentifier(char c) {
            return std::isalpha(c) || c == '_' || c == '$' || std::isdigit(c);
        }

        static bool isSpace(char c) {
            return std::isspace(c);
        }
    };
    struct Variable {
        std::string name;
        State state = State::UNDEF;

        explicit Variable(std::string name_,State state_ = State::UNDEF) : name(std::move(name_)), state(state_) {
            //empty
        }
    };
    class Module;
    class Command {
    public:
        std::string moduleName;
        std::string gateName;
        std::vector<std::string> arguments;
        Module *module = nullptr;
        Module *parent = nullptr;

        Command() = default;
        Command(const Command &command) : moduleName(command.moduleName),
                                                   gateName(command.gateName), arguments(command.arguments),
                                                   module(nullptr), parent(command.parent) {
            //empty
        }

        Command(Command &&command)  noexcept : moduleName(std::move(command.moduleName)),
                                                        gateName(std::move(command.gateName)), arguments(std::move(command.arguments)),
                                                        module(command.module), parent(command.parent) {
            command.module = nullptr;
        }

        Command &operator=(const Command &command) {
            if(&command == this)
                return *this;
            moduleName = command.moduleName;
            gateName = command.gateName;
            arguments = command.arguments;
            module = nullptr;
            parent = command.parent;
            return *this;
        }

        Command &operator=(Command &&command) noexcept {
            if(&command == this)
                return *this;
            moduleName = std::move(command.moduleName);
            gateName = std::move(command.gateName);
            arguments = std::move(command.arguments);
            module = command.module;
            parent = command.parent;

            command.module = nullptr;
            return *this;
        }

        ~Command() noexcept;

        void run();
        std::vector<std::shared_ptr<Variable>> makeArguments();

    private:
        static void nand(std::vector<std::shared_ptr<Variable>> &args) {
            auto out = args.front();
            auto in0 = args.at(1);
            auto in1 = args.at(2);
            if(in0->state == State::HIGH && in1->state == State::HIGH){
                out->state = State::LOW;
            }else{
                out->state = State::HIGH;
            }
        }

        static void buf(std::vector<std::shared_ptr<Variable>> &args) {
            auto in = args.front();
            auto out = args.at(1);
            in->state = out->state;
        }

        static void bufif1(std::vector<std::shared_ptr<Variable>> &args) {
            auto out = args.front();
            auto in = args.at(1);
            auto control = args.at(2);

            if(control->state != State::HIGH || in->state == State::UNDEF){
                out->state = State::UNDEF;
            }else{
                out->state = in->state;
            }
        }

    };
    class Module {
    public:
        static std::set<std::string> modulesCalled;
        static std::vector<Module> modules;
        std::string name;
        std::vector<std::string> params;
        std::vector<Command> commands;
        std::map<std::string,std::shared_ptr<Variable>> vars;

        Module() = default;
        Module(const Module &module);
        Module(Module &&module) noexcept;

        Module &operator=(const Module &module);
        Module &operator=(Module &&module) noexcept;

        void setCommandsParent();

        void compile(Lexer::Lexed::iterator &it,Lexer::Lexed::iterator end);
        void run(std::vector<std::shared_ptr<Variable>> &arguments);

        Module instance();
        Module *pointerInstance();

        static Module *find(const std::string &moduleName);
    };
    class Runner {
    public:
        static void run(const std::string &moduleName,std::vector<std::shared_ptr<Variable>> &arguments);
        static void compile(const std::string &code);
    };
} // verilog

#endif //VERILOG_HPP