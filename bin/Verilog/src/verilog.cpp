//
// Created by a404m on 3/22/23.
//

#include "verilog.h"

namespace verilog {
    std::set<std::string> Module::modulesCalled{};
    std::vector<Module> Module::modules{};

    Command::~Command() noexcept {
        delete module;
    }

    void Command::run() {
        auto args = makeArguments();
        if(moduleName == "nand"){
        	while(args.size() < 3){
        		args.push_back(std::make_shared<Variable>(""));
        	}
            nand(args);
            return;
        }else if(moduleName == "buf"){
        	while(args.size() < 2){
        		args.push_back(std::make_shared<Variable>(""));
        	}
            buf(args);
            return;
        }else if(moduleName == "bufif1"){
        	while(args.size() < 3){
        		args.push_back(std::make_shared<Variable>(""));
        	}
            bufif1(args);
            return;
        }
        if(module == nullptr){
            auto m = Module::find(moduleName);
            if(m == nullptr){
                throw std::runtime_error("no module found '"+moduleName+"'");
            }
            module = new Module(m->instance());
        }
        module->run(args);
    }

    std::vector<std::shared_ptr<Variable>> Command::makeArguments() {
        std::vector<std::shared_ptr<Variable>> args;
        for(const auto &argName : arguments){
            if(argName.empty()){
                args.push_back(std::make_shared<Variable>(""));
            }
            std::shared_ptr<Variable> var = parent->vars[argName];
            if(var == nullptr){
                var = parent->vars[argName] = std::make_shared<Variable>(argName);
            }
            args.push_back(var);
        }
        return args;
    }

    void swap(Command &cmd0,Command &cmd1){
        using std::swap;
        swap(cmd0.moduleName,cmd1.moduleName);
        swap(cmd0.gateName,cmd1.gateName);
        swap(cmd0.arguments,cmd1.arguments);
        swap(cmd0.module,cmd1.module);
        swap(cmd0.parent,cmd1.parent);
    }

    void Module::run(std::vector<std::shared_ptr<Variable>> &arguments) {
        if(modulesCalled.find(name) != modulesCalled.end()){
        	std::string error = "infinite module loop by calling '"+name+"'\n";
        	for(const auto &moduleCalled : modulesCalled){
        		error += moduleCalled+"\n";
        	}
            throw std::runtime_error(error);
        }else{
            modulesCalled.emplace(name);
        }
    	while(arguments.size() < params.size()){
    		arguments.push_back(std::make_shared<Variable>(""));
    	}
        for(int i = 0;i < arguments.size();i++){
            vars[params.at(i)] = arguments[i];
        }
        for(auto &command : commands){
            command.run();
        }
        modulesCalled.erase(name);
    }

    void Module::compile(Lexer::Lexed::iterator &it,Lexer::Lexed::iterator end) {
        name = *++it;
        ++it;// name
        ++it;// "("
        int paramNum = 0;
        params.emplace_back();
        for(;it < end && *it != ")";++it){
            if(*it == ",") {
                ++paramNum;
                params.emplace_back();
                ++it;
            }
            if(*it == ")")
                break;
            params.back() = *it;
        }
        params.resize(paramNum+1);
        ++it;// ")"
        ++it;// ";"
        for(;it < end;++it){
            if(*it == "endmodule"){
                ++it;// ";"
                break;
            }else if(*it == "output" || *it == "input" || *it == "wire"){
                it = std::find(it,end,";");
            }else{
                Command command;
                command.moduleName = *it;
                ++it;
                command.gateName = *it;
                ++it;
                ++it;// "("
                command.parent = this;
                for(;it < end && *it != ")";++it){
                    if(*it == ",") {
                        if(*(it-1) == ","){
                            command.arguments.emplace_back();
                        }
                        continue;
                    }
                    command.arguments.push_back(*it);
                }
                ++it;// ")"
                commands.push_back(command);
            }
        }
    }

    Module Module::instance() {
        return {*this};
    }

    Module *Module::pointerInstance(){
        return new Module(*this);
    }

    Module *Module::find(const std::string &moduleName) {
        for(auto &module : modules){
            if(module.name == moduleName){
                return &module;
            }
        }
        return nullptr;
    }

    Module::Module(const Module &module) : name(module.name), params(module.params), commands(module.commands), vars() {
        setCommandsParent();
    }

    Module::Module(Module &&module) noexcept : name(std::move(module.name)), params(std::move(module.params)),
                                      commands(std::move(module.commands)), vars(std::move(module.vars)) {
        setCommandsParent();
    }

    Module &Module::operator=(const Module &module) {
        if(&module == this)
            return *this;
        name = module.name;
        params = module.params;
        commands = module.commands;
        setCommandsParent();
        vars = {};
        return *this;
    }

    Module &Module::operator=(Module &&module)  noexcept {
        if(&module == this)
            return *this;
        name = std::move(module.name);
        params = std::move(module.params);
        commands = std::move(module.commands);
        setCommandsParent();
        vars = std::move(module.vars);
        return *this;
    }

    void Module::setCommandsParent() {
        for(auto &cmd : commands){
            cmd.parent = this;
        }
    }

    void Runner::run(const std::string &moduleName,std::vector<std::shared_ptr<Variable>> &arguments) {
        for(auto &module : Module::modules){
            if(moduleName == module.name) {
                module.run(arguments);
                return;
            }
        }
        throw std::runtime_error("module not found '"+moduleName+"'");
    }

    void Runner::compile(const std::string &code) {
        auto lexed = Lexer::lex(code);
        for(auto it = lexed.begin(),end = lexed.end();it < end;++it){
            const auto &word = *it;
            if(word == "module"){
                Module module;
                module.compile(it,end);
                auto place = Module::find(module.name);
                if(place == nullptr)
                    Module::modules.emplace_back(std::move(module));
                else
                    *place = std::move(module);
            }else{
                throw std::runtime_error("bad code " + word);
            }
        }
    }
} //verilog
