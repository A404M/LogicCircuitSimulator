# Logic Gate Simulator  

This program is a simple digital logic simulator.  

You can make gates and use them to make other gates.  
The base of all gates are nand,buf(buffer),bufif1(tri-state buffer enable high).  
You can save the gates on db by simply pressing save button on board menu.  

Made by Godot 3.5  

---

## TODO LIST(starred ones has higher priority):  
- Ability to make separated workflows
- Ability to set gate colors on saving
- Adding 7-segment display
- Adding bitmap display(like regular displays)
- Ability to move board input and output vertically*
- Ability to connect wires together at any point(not only at IOs)
- Ability to add gates with code(my simplified Verilog)
- Ability to exporting workflows or gates easily
- Adding documentation*
- Porting to Godot 4(maybe after Godot 4.1 involved)

---

You can download binary release [here](https://a404m.itch.io/logiccircuitsimulator)

---

Feel free to open an issue or feedback
