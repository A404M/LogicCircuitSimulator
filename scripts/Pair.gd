class_name Pair

var first
var second

func _init(f = null,s = null):
	self.first = f
	self.second = s


func serialize():
	return {
		"first":first,
		"second":second
	}


func deserialize(dict):
	first = dict.first
	second = dict.second
	return self
