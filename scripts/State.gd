class_name State

enum Values{
	UNDEF = -1,
	LOW = 0,
	HIGH = 1
}

const UNDEF := Values.UNDEF
const LOW := Values.LOW
const HIGH := Values.HIGH

var state := UNDEF

func _init(state_ := UNDEF):
	state = state_
