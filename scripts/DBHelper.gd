class_name DBHelper

const SQLiteWrapper = preload("res://addons/godot-sqlite/godot-sqlite-wrapper.gd")
const Gate = preload("res://scenes/gate/Gate.tscn")

const BOARD_TABLE := "boards"
const BOARD_TABLE_NAME = "name"
const BOARD_TABLE_DATA = "data"
const BOARD_TABLE_CODE = "code"

const DB_FILE_PATH = "res://data/lcsData.db"
const DB_PATH = "user://data/lcsData.db"

var db:SQLiteWrapper

func _init():
	var dir = Directory.new()
	dir.open("user://")
	if not dir.dir_exists("data"):
		copyDB()
		
	open()


func copyDB():
	var dir := Directory.new()
	dir.open("user://")
	dir.make_dir("user://data/")
	dir.copy(DB_FILE_PATH,DB_PATH)


func open()->void:
	db = SQLiteWrapper.new()
	db.path = DB_PATH
	db.verbosity_level = 1#0->Quiet 1->Normal 2->Verbose 3->Very Verbose (Error showing level)
	db.open_db()
	
	if(not tableExists(BOARD_TABLE)):
		createBoardTable()


func close()->void:
	db.close_db()

func createBoardTable()->void:
	var tableDict : Dictionary = {
		BOARD_TABLE_NAME:{"data_type":"text", "primary_key": true, "not_null": true},
		BOARD_TABLE_DATA:{"data_type":"text", "not_null": true},
		BOARD_TABLE_CODE:{"data_type":"text", "not_null": true}
	}
	db.create_table(BOARD_TABLE,tableDict)


func boardNameExists(name:String)->bool:
	#db.query('SELECT EXISTS(SELECT 1 FROM %s WHERE %s="%s" LIMIT 1);'%[BOARD_TABLE,BOARD_TABLE_NAME,name])
	#return db.query_result.size() != 0
	return selectBoard(name) != null


func tableExists(name:String)->bool:
	if name in ["nand","buf","bufif1"]:
		return true
	db.query('SELECT name FROM sqlite_master WHERE type="table" AND name="%s";'%name)
	return db.query_result.size() != 0


func insertBoard(name:String,data:Dictionary,code:String)->bool:
	var dict = {
		BOARD_TABLE_NAME:name,
		BOARD_TABLE_DATA:var2str(data),
		BOARD_TABLE_CODE:code
	}
	if boardNameExists(dict.name):
		return false
	return db.insert_row(BOARD_TABLE,dict)


func selectBoard(name:String):
	var condition = "`%s`='%s'"%[BOARD_TABLE_NAME,name]
	var array_result:Array = db.select_rows(BOARD_TABLE,condition,["*"])
	if array_result.size() == 1:
		array_result[0].data = str2var(array_result[0].data)
		return array_result[0] #it's a dictionary
	else:
		return null


func getBoardAsGate(name:String):
	var board = selectBoard(name)
	if board == null:
		return null
	else:
		return Gate.instance().setAll(
			"888888",
			board.data.boardName,
			board.data.inputs.size(),
			board.data.outputs.size()
		)


func updateBoardName(from:String,to:String)->bool:
	var condition = "`%s`='%s'"%[BOARD_TABLE_NAME,from]
	return db.update_rows(BOARD_TABLE,condition,{
			BOARD_TABLE_NAME:to
		}
	)


func updateBoard(name:String,newData:Dictionary,newCode:String)->bool:
	var condition = "`%s`='%s'"%[BOARD_TABLE_NAME,name]
	return db.update_rows(BOARD_TABLE,condition,{
			BOARD_TABLE_DATA:var2str(newData),
			BOARD_TABLE_CODE:newCode
		}
	)


func deleteBoard(name:String)->bool:
	var condition = "`%s`='%s'"%[BOARD_TABLE_NAME,name]
	return db.delete_rows(BOARD_TABLE,condition)


func insertIfNotExists(name:String,newData:Dictionary,newCode:String)->bool:
	if(boardNameExists(name)):
		return updateBoard(name,newData,newCode)
	else:
		return insertBoard(name,newData,newCode)


func boardNames()->Array:
	var results = db.select_rows(BOARD_TABLE,"1",[BOARD_TABLE_NAME])
	var ret = []
	for result in results:
		ret.push_back(result[BOARD_TABLE_NAME])
	return ret
