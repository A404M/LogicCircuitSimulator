class_name SceneHelper

static func changeScene(var current:Node,var next:Node)->void:
	var parent = current.get_parent()
	current.queue_free()
	parent.add_child_below_node(parent.get_child(0),next)
	#parent.add_child(next)
	next.set_owner(parent)


static func freeChilds(var node:Node)->void:
	for child in node.get_children():
		node.remove_child(child)
		child.queue_free()


static func removeNumericNamedChild(var parent:Node,var child:Node)->void:
	parent.remove_child(child)
	var num = int(child.name)
	child.queue_free()
	for node in parent.get_children():
		var n = int(node.name)
		if n > num:
			node.name = str(n-1)
