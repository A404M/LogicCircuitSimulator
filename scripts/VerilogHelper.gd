class_name VerilogHelper

#const Verilog = preload("res://native_script/Verilog.gdns")

static func run(board,verilog,lastCode)->String:
	var code = convertToVerilog(board)
	if lastCode != code:
		#print(code)
		if verilog.compile(code) != OK:
			print("bad code")
			return ""
	var outs = verilog.run(board.boardName.to_lower(),getInputs(board))
	if typeof(outs) != TYPE_ARRAY:
		print("bad running")
		return ""
	var outputs = board.getOutputs()
	for i in range(outputs.size()):
		var out = outs[i]
		var output = outputs[i]
		output.setState(out)
	return code

static func getInputs(board)->Array:
	var inputs = []
	for input in board.getInputs():
		inputs.push_back(input.getState())
	return inputs

static func convertToVerilog(board)->String:
	var outputList := ""
	for output in board.getOutputs():
		outputList += "o"+output.name+","
	if not outputList.empty():
		outputList = outputList.substr(0,outputList.length()-1)
	var inputList := ""
	for input in board.getInputs():
		inputList += "i"+input.name+","
	if not inputList.empty():
		inputList = inputList.substr(0,inputList.length()-1)
	var wireList := ""
	for wire in board.getWires():
		wireList += "w"+wire.name+","
	if not wireList.empty():
		wireList = wireList.substr(0,wireList.length()-1)
	
	var code:String = "module %s("%board.boardName.to_lower()
	code += outputList
	if outputList != "" and inputList != "":
		code += ","
	code += inputList + ");\n"
	
	if outputList != "":
		code += "output %s;\n"%outputList
	
	if inputList != "":
		code += "input %s;\n"%inputList
	
	if wireList != "":
		code += "wire %s;\n"%wireList
	
	for input in board.getInputs():
		var wire = input.getWire()
		if wire == null:
			continue
		code += "buf %s(%s,%s);\n"%["bi"+input.name,"w"+wire.name,"i"+input.name]
	
	for gate in board.getGates():
		var wires := ""
		for output in gate.getOutputs():
			var wire = output.getWire()
			if wire == null:
				continue
			wires += "w"+(wire.name if wire != null else "")+","
		for input in gate.getInputs():
			var wire = input.getWire()
			if wire == null:
				continue
			wires += "w"+(wire.name if wire != null else "")+","
		if wires != "":
			wires = wires.substr(0,wires.length()-1)
			
		code += gate.gateName.to_lower()+" g"+gate.name+"("+wires+");\n"
	
	for output in board.getOutputs():
		var wire = output.getWire()
		if wire == null:
			continue
		code += "buf %s(%s,%s);\n"%["bo"+output.name,"o"+output.name,"w"+wire.name]
	code += "endmodule;\n"
	return code
