extends Control

class_name Wire

const Connection = preload("res://scenes/wire/connection/Connection.tscn")
var GateIO = load("res://scenes/gate/gate_io/GateIO.gd")

export var color:Color = Color('555555') setget setColor
export var width:float = 10 setget setWidth

var pointBranches := []
var rightClickedBranchIndex := -1

func _draw():
	drawAllLines()

func _exit_tree():
	for pointBranch in pointBranches:
		for point in pointBranch:
			if point.get_parent() is GateIO:
				var io = point.get_parent()
				if io.getWire() == self:
					io.setWire(null)
	queue_free()


func remove():
	SceneHelper.removeNumericNamedChild(get_parent(),self)


func removeIfShould():
	if pointBranches.empty():
		print("should be removed")
		remove()


func calcRawPosition(pos:Vector2)->Vector2:
	return pos-get_parent().get_global_rect().position

func calcPosition(node:Control)->Vector2:
	return calcRawPosition(node.get_global_rect().position)

func drawAllLines():
	SceneHelper.freeChilds($LineHolder)
	for pointBranch in pointBranches:
		for i in range(pointBranch.size()-1):
			var line := Line2D.new()
			$LineHolder.add_child(line)
			line.set_owner($LineHolder)
			line.points = getPoints(calcPosition(pointBranch[i]),calcPosition(pointBranch[i+1]))
			line.width = width
			line.default_color = color


static func getPoints(p0:Vector2,p1:Vector2)->Array:
	if true or is_equal_approx(p0.x,p1.x) or is_equal_approx(p0.y,p1.y):
		return [p0,p1]
	else:
		var dx := abs(p0.x-p1.x)
		var dy := abs(p0.y-p1.y)
		if dx >= dy:
			return [p0,Vector2(p1.x,p0.y)]
		else:
			return [p0,Vector2(p0.x,p1.y)]
		#return [p0,Vector2(p0.x,p1.y),p1]


func setColor(val):
	color = val
	update()


func setWidth(val):
	width = val
	update()

func _input(event):
	inputHandler(event)

func inputHandler(event):
	if event.is_action_pressed("mouse_right_click"):
		if name != "HintWire" and isPositionIn((event as InputEventMouse).global_position):
			$RightClickMenu.popup()
			var pos = get_global_mouse_position()
			var p0 = $RightClickMenu.rect_size
			var win = OS.window_size
			if pos.x + p0.x >= win.x:
				pos.x = win.x-p0.x
			
			if pos.y + p0.y >= win.y:
				pos.y = win.y-p0.y
			$RightClickMenu.set_global_position(pos)
			get_tree().get_root().set_input_as_handled()
			#remove()
	#elif event.is_action_pressed("mouse_left_click"):
	#	$RightClickMenu.hide()


func isPositionIn(pos:Vector2)->bool:
	for i in range(pointBranches.size()):
		var points = pointBranches[i]
		for j in range(points.size()-1):
			var p0 = points[j].rect_global_position
			var p1 = points[j+1].rect_global_position
			if isBetween(pos,p0,p1):
				rightClickedBranchIndex = i
				return true
	return false


func isBetween(pos:Vector2,p0:Vector2,p1:Vector2)->bool:
	var squared_width := width*width
	var closest_point := Geometry.get_closest_point_to_segment_2d(pos, p0, p1)
	
	return closest_point.distance_squared_to(pos) <= squared_width

func addPoint(globalPos:Vector2)->void:
	var point = Connection.instance()
	connectConnection(point)
	getPointHolder().add_child(point)
	point.set_owner(getPointHolder())
	point.set_global_position(globalPos)
	
	if pointBranches.empty():
		pointBranches.push_back([point])
	elif pointBranches.back().back().get_parent() is GateIO:
		pointBranches.push_back([pointBranches.back().back(),point])
	else:
		pointBranches.back().push_back(point)
	

func addIO(io,hintWire:Wire = null,after = null)->void:
	var newPoints := []
	if hintWire != null:
		for point in hintWire.pointBranches[-1]:
			var newPoint := toPoint(point)
			if newPoint.get_parent() is GateIO:
				newPoint.get_parent().setWire(self)
			newPoints.push_back(newPoint)
	
	newPoints.push_back(io.getCenterNode())
	io.setWire(self)
	if after == null:
		if pointBranches.empty():
			pointBranches.push_back([])
		pointBranches.back().append_array(newPoints)
	else:
		newPoints.push_front(after)
		pointBranches.push_back(newPoints)


func branchEndingWith(node:Control)->Array:
	for branch in pointBranches:
		if not branch.empty() and branch.back() == node:
			return branch
	pointBranches.push_back([])
	return pointBranches.back()

func connectToWire(wire:Wire,after:Control,hintWire:Wire):
	# a line connecting two wires according to hintWire
	for branch in hintWire.pointBranches:
		var newPoints := []
		for point in branch:
			newPoints.push_back(toPoint(point))
		pointBranches.push_back(newPoints)
	if wire == self:
		update()
		return
	# adding pointBranches from from wire to self
	for pointBranch in wire.pointBranches:
		var newPoints := []
		for point in pointBranch:
			if point.get_parent() == wire.getPointHolder():
				newPoints.push_back(toPoint(point))
			else:
				newPoints.push_back(point)
				point.get_parent().setWire(self)
	
	wire.remove()
	update()


func toPoint(point:Control)->Control:
	if point.get_parent() is GateIO:
		return point
	else:
		var newPoint = Connection.instance()
		connectConnection(newPoint)
		getPointHolder().add_child(newPoint)
		newPoint.set_owner(getPointHolder())
		newPoint.set_global_position(point.get_global_rect().position)
		return newPoint


func serialize():
	#var gateIOs = []
	#for io in ios:
	#	gateIOs.push_back(io.get_path())
	var tempPoints := []
	for pointBranch in pointBranches:
		var tempBranch := []
		for point in pointBranch:
			tempBranch.push_back(point.get_path())
		tempPoints.push_back(tempBranch)
	#for point in points:
	#	tempPoints.push_back(point.get_path())
	var connections := []
	for connection in getPointHolder().get_children():
		connections.push_back({
			"name":connection.name,
			"pos_x":connection.get_global_rect().position.x,
			"pos_y":connection.get_global_rect().position.y
			})
	return {
		"color":color,
		"width":width,
		"connections":connections,
		#"ios":gateIOs,
		"pointBranches":tempPoints
		#"board":board.get_path()
	}


func deserialize(dict,parent:Node):
	parent.add_child(self)
	self.name = String(parent.get_child_count())
	set_owner(parent)
	color = dict.color
	width = dict.width
	for connection in dict.connections:
		var conn := Connection.instance()
		connectConnection(conn)
		conn.name = connection.name
		getPointHolder().add_child(conn)
		conn.set_owner(getPointHolder())
		conn.set_global_position(Vector2(connection.pos_x,connection.pos_y))
	#ios.clear()
	#for io in dict.ios:
	#	ios.push_back(get_node(io))
	#	get_node(io).wire = self
	#points.clear()
	clearAllPoints()
	for pointBranch in dict.pointBranches:
		var newBranch := []
		for point in pointBranch:
			var node = get_node(point)
			newBranch.push_back(node)
			if node.get_parent() is GateIO:
				node.get_parent().setWire(self)
		pointBranches.push_back(newBranch)
	update()
	return self

func clearAllPoints():
	pointBranches.clear()
	SceneHelper.freeChilds(getPointHolder())

func getLastPoint():
	return pointBranches[-1][-1]

func setLastPoint(point:Control):
	if point == pointBranches[-1][-1]:
		return
	getPointHolder().remove_child(pointBranches[-1][-1])
	pointBranches[-1][-1] = point

func clear():
	clearAllPoints()
	update()
	#points.clear()


func getPointHolder()->Node:
	return $Connections


func connectConnection(conn:Connection):
	pass
#	conn.connect("clicked",self,"_on_Connection_clicked")
#	conn.connect("rightClicked",self,"_on_Connection_rightClicked")
#
#
#func _on_Connection_clicked():
#	pass
#
#
#func _on_Connection_rightClicked():
#	$RightClickMenu.hide()


func reverse()->Wire:
	for branch in pointBranches:
		for i in range(int(branch.size()/2)):
			var temp = branch[i]
			branch[i] = branch[branch.size()-i-1]
			branch[branch.size()-i-1] = temp
	return self


func removeBranch(index:int)->void:
	var branch = pointBranches[index]
	pointBranches.remove(index)
	for point in branch:
		if point.get_parent() is GateIO:
			if not has(point) and point.get_parent().getWire() == self:
				point.get_parent().setWire(null)
		else:
			$Connections.remove_child(point)
			point.queue_free()
	update()


func has(node:Node)->bool:
	for points in pointBranches:
		if node in points:
			return true
	return false


func _on_RightClickMenu_id_pressed(id:int):
	match(id):
		0:#delete
			removeBranch(rightClickedBranchIndex)
			removeIfShould()
