tool

class_name Connection

extends Control

signal clicked
signal rightClicked

export var radius:float = 5 setget setRadius
export var color:Color = Color('555555') setget setColor

#func _ready():
#	hide()

func _draw():
	draw_circle(Vector2.ZERO,radius,color)
	#set_size(Vector2(radius*2,radius*2))



func _input(event:InputEvent):
	if not visible or get_parent().get_parent().name == "HintWire":
		return
#	if event.is_action_pressed("mouse_left_click"):
#		var pos = (event as InputEventMouseButton).position
#		if isIn(pos):
#			emit_signal("clicked")
#			print("clicked")
#			get_tree().get_root().set_input_as_handled()
#	elif event.is_action_pressed("mouse_right_click"):
#		var pos = (event as InputEventMouseButton).position
#		if isIn(pos):
#			emit_signal("rightClicked")
#			print("rightClicked")
#			get_tree().get_root().set_input_as_handled()
	
	draggingInput(event)
func isIn(pos:Vector2)->bool:
	pos-=rect_global_position
	pos = Vector2(abs(pos.x),abs(pos.y))
	return pos.x <= radius and pos.y <= radius


func setRadius(val):
	radius = val
	update()


func setColor(val):
	color = val
	update()


func serialize():
	return {
		"radius":radius,
		"color":color,
		"posX":rect_global_position.x,
		"posY":rect_global_position.y
	}


func deserialize(dict):
	radius = dict.radius
	color = dict.color
	set_global_position(dict.posX,dict.posY)
	update()
	return self


#dragging

var dragging = false

#var tsize = Vector2()

var offset = Vector2.ZERO

#func ready():
#	tsize = Vector2(get_rect().size.x,get_rect().size.y)
#	rect_position = get_rect().size / 2


func _process(_delta):
	var sheet = get_parent().get_parent().get_parent()
	if dragging:
		var newPos = get_global_mouse_position() + offset
		var mp = sheet.get_local_mouse_position()
		var pp = sheet.rect_size
		var off = get_local_mouse_position()
		var s = rect_size
		var endOff = s-off
		if (mp-off).x < 0:
			newPos.x = 1
			dragging = false
		elif (mp+endOff).x > pp.x:
			newPos.x = pp.x-s.x-1
			dragging = false
		if (mp-off).y < 0:
			newPos.y = 1
			dragging = false
		elif (mp+endOff).y > pp.y:
			newPos.y = pp.y-s.y-1
			dragging = false
		
		rect_position = newPos
		
		get_parent().get_parent().update()


func draggingInput(event:InputEvent):
	if event is InputEventMouseButton and isIn(event.position):
		dragging = event.is_action_pressed("mouse_left_click")
		offset = rect_position - event.global_position
#		if dragging:
#			get_tree().get_root().set_input_as_handled()
