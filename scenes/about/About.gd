extends Control

var MainMenu = load("res://scenes/main_menu/MainMenu.tscn")

func _ready():
	$Label.bbcode_text = "[center]"+$Label.bbcode_text+"[/center]" + "\n\n\n" + readFile("res://LICENSE")
	$Label.bbcode_enabled = true

func readFile(fileName:String)->String:
	var file := File.new()
	file.open(fileName,File.READ)
	return file.get_as_text()


func _input(event:InputEvent):
	if event.is_action_pressed("cancel"):
		#get_tree().get_root().set_input_as_handled()
		SceneHelper.changeScene(self,MainMenu.instance())
