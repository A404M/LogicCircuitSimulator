tool

extends Control

signal nameEntered(nameEntered)
signal canceled

export var hint:String = "" setget setHint

func _on_LineEdit_text_changed(new_text:String):
	setError("")
	var text := ""
	for c in new_text:
		if c.is_valid_identifier() or c.is_valid_integer():
			text += c
	
	$LineEdit.text = text
	$LineEdit.caret_position = text.length()


func _on_LineEdit_text_entered(new_text):
	if new_text == "":
		return
	emit_signal("nameEntered",new_text)


func setHint(val:String):
	hint = val
	$LineEdit.placeholder_text = hint

func setError(val:String):
	$Error.text = val

func _input(event:InputEvent):
	if event.is_action_pressed("cancel"):
		emit_signal("canceled")
		#get_tree().get_root().set_input_as_handled()
