extends Control

class_name BoardOutput

signal ioClicked(node,isInput,i)
signal delete

func _on_BoardIO_ioClicked(node, isInput, i):
	emit_signal("ioClicked",node,false,i)


func serialize():
	return {
		"BoardIO":$BoardIO.serialize()
	}

func deserialize(dict):
	$BoardIO.deserialize(dict.BoardIO)
	update()
	return self

func setState(val):
	$BoardIO.setState(val)

func _on_BoardIO_rightClicked():
	$RightClickMenu.popup()
	var pos = get_global_mouse_position()
	var p0 = $RightClickMenu.rect_size
	var win = OS.window_size
	if pos.x + p0.x >= win.x:
		pos.x = win.x-p0.x
	
	if pos.y + p0.y >= win.y:
		pos.y = win.y-p0.y
	
	$RightClickMenu.set_global_position(pos)
	#get_parent().get_parent().removeOutput(self)


func getWire():
	return $BoardIO/GateIO.getWire()


func _on_RightClickMenu_id_pressed(id:int):
	match(id):
		0:#delete
			emit_signal("delete")


func setIsHint(val:bool):
	$BoardIO.isHint = val


func getIsHint():
	return $BoardIO.isHint
