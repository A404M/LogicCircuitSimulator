tool

class_name BoardTop

extends Control

const BoardInput = preload("res://scenes/board/board_top/board_input/BoardInput.tscn")
const BoardOutput = preload("res://scenes/board/board_top/board_output/BoardOutput.tscn")
#const WireGD = preload("res://scenes/wire/Wire.gd")

export var border:Color = Color('2c2c2c') setget setBorder
export var background:Color = Color('2d2d33') setget setBackground
export var hintColor:Color = Color('308351')
export var hintWidth:float = 4

const ioSep = 12*2+4

var clickedIO:GateIO = null
var boardName := ""


func clear():
	#ioSep = 12*2+4
	clickedIO = null
	boardName = ""
	$Sheet.clear()
	removeInputs()
	removeOutputs()
	_ready()


func _ready():
	addInput(BoardInput.instance())
	addOutput(BoardOutput.instance())


func _draw():
	$Border/ColorRect.color = border
	$Sheet.background = background
	$Inputs.add_constant_override("separation", ioSep)
	$Outputs.add_constant_override("separation", ioSep)


func _input(event):
	if clickedIO != null and event is InputEventMouseMotion:
		var globalPos := get_global_mouse_position()
		if Input.is_action_pressed("shift_key"):
			var p0 := $HintWire.pointBranches[0][-2].get_global_rect().position as Vector2
			var p1 := globalPos
			var dx = abs(p0.x-p1.x)
			var dy = abs(p0.y-p1.y)
			if dx >= dy:
				globalPos = Vector2(p1.x,p0.y)
			else:
				globalPos = Vector2(p0.x,p1.y)
			
		get_tree().get_root().set_input_as_handled()
		$HintWire.getLastPoint().set_global_position(globalPos)
		$HintWire.update()
	elif event.is_action_pressed("cancel"):
		print("here")
		clickedIO = null
		#get_tree().get_root().set_input_as_handled()
		$HintWire.clear()
		$HintWire.update()


func setBorder(val):
	border = val
	update()


func setBackground(val):
	background = val
	update()


func _on_IO_ioClicked(node:GateIO,isInput,i):
	if node == clickedIO:
		pass
	elif clickedIO == null:
		clickedIO = node
		$HintWire.addPoint(Vector2.ZERO)
		$HintWire.setLastPoint(clickedIO.getCenterNode())
		$HintWire.addPoint(get_global_mouse_position())
	else:
		$HintWire.setLastPoint(node.getCenterNode())
		clickedIO.wireTo(node,$Sheet/WireHolder,$HintWire)
		clickedIO = null
		$HintWire.clear()


func _on_Gate_clicked():
	clickedIO = null
	$HintWire.clear()


func addInput(input:BoardInput,y = get_local_mouse_position().y):
	$Inputs.add_child(input)
	input.name = str($Inputs.get_child_count())
	input.connect("ioClicked",self,"_on_IO_ioClicked")
	input.connect("delete",self,"_on_Input_delete",[input])
	input.set_owner($Inputs)

func removeInput(input:BoardInput):
	SceneHelper.removeNumericNamedChild($Inputs,input)

func removeInputs():
	SceneHelper.freeChilds($Inputs)

func addOutput(output:BoardOutput):
	$Outputs.add_child(output)
	output.name = str($Outputs.get_child_count())
	output.connect("ioClicked",self,"_on_IO_ioClicked")
	output.connect("delete",self,"_on_Output_delete",[output])
	output.set_owner($Outputs)

func removeOutput(output:BoardOutput):
	SceneHelper.removeNumericNamedChild($Outputs,output)

func removeOutputs():
	SceneHelper.freeChilds($Outputs)

#func removeWire(wire:Wire):
	#wire.first.removeWire()
	#wire.second.removeWire()
	#SceneHelper.removeNumericNamedChild($Sheet/WireHolder,wire)

func getInputStates()->Array:
	var inputs = []
	for input in $Inputs.get_children():
		inputs.push_back(input.getState())
	return inputs

func getGates()->Array:
	return $Sheet.getGates()

func getWires()->Array:
	return $Sheet.getWires()

func getInputs()->Array:
	return $Inputs.get_children()

func getOutputs()->Array:
	return $Outputs.get_children()


func serialize():
	var inputs = []
	var outputs = []
	for input in $Inputs.get_children():
		inputs.append(input.serialize())
	for output in $Outputs.get_children():
		outputs.append(output.serialize())
	return {
		"boardName":boardName,
		"border":border,
		"background":background,
		"hintColor":hintColor,
		"hintWidth":hintWidth,
		#"ioSep":ioSep,
		"inputs":inputs,
		"outputs":outputs,
		"Sheet":$Sheet.serialize()
	}


func deserialize(dict):
	SceneHelper.freeChilds($Inputs)
	SceneHelper.freeChilds($Outputs)
	boardName = dict.boardName
	border = dict.border
	background = dict.background
	hintColor = dict.hintColor
	hintWidth = dict.hintWidth
	#ioSep = dict.ioSep
	for input in dict.inputs:
		var inp = BoardInput.instance()
		addInput(inp)
		inp.deserialize(input)
	for output in dict.outputs:
		var out = BoardOutput.instance()
		addOutput(out)
		out.deserialize(output)
	$Sheet.deserialize(dict.Sheet)
	update()
	return self


func _on_Inputs_gui_input(event:InputEvent):
	if event.is_action_pressed("mouse_left_click"):
		get_tree().get_root().set_input_as_handled()
		getInputs().back().setIsHint(false)


func _on_Outputs_gui_input(event):
	if event.is_action_pressed("mouse_left_click"):
		get_tree().get_root().set_input_as_handled()
		getOutputs().back().setIsHint(false)


func _on_Sheet_clicked(globalPos:Vector2):
	if clickedIO != null:
		if Input.is_action_pressed("shift_key"):
			var p0 := $HintWire.pointBranches[0][-2].get_global_rect().position as Vector2
			var p1 := globalPos
			var dx = abs(p0.x-p1.x)
			var dy = abs(p0.y-p1.y)
			if dx >= dy:
				globalPos = Vector2(p1.x,p0.y)
			else:
				globalPos = Vector2(p0.x,p1.y)
			
		var last:Control = $HintWire.getLastPoint()
		$HintWire.addPoint(globalPos)
		var newLast:Control = $HintWire.getLastPoint()
		var temp := last.rect_global_position
		last.set_global_position(newLast.rect_global_position)
		newLast.set_global_position(temp)

func _on_Input_delete(node:BoardInput):
	removeInput(node)


func _on_Output_delete(node:BoardOutput):
	removeOutput(node)


func _on_Inputs_mouse_entered():
	var input = BoardInput.instance()
	addInput(input)
	input.setIsHint(true)


func _on_Inputs_mouse_exited():
	for input in $Inputs.get_children():
		if input.getIsHint():
			removeInput(input)
			return


func _on_Outputs_mouse_entered():
	var output = BoardOutput.instance()
	addOutput(output)
	output.setIsHint(true)


func _on_Outputs_mouse_exited():
	for output in $Outputs.get_children():
		if output.getIsHint():
			removeOutput(output)
			return
