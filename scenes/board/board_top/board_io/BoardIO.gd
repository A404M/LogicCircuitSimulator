tool

class_name BoardIO

extends Control

signal ioClicked(node,isInput,i)
signal clicked
signal rightClicked

const State = preload("res://scripts/State.gd")

export var radius0:float = 12 setget setRadius0
export var radius1:float = 8 setget setRadius1
export var lineLength:float = 20 setget setLineLength
export var lineWidth:float = 5 setget setLineWidth
export var color:Color = Color('232323') setget setColor
export var focusedColor:Color = Color('888888') setget setFocusedColor
export var highColor:Color = Color('d81b1b') setget setHighColor
export var lowColor:Color = Color('1b1bd8') setget setLowColor
export var isInput:bool = true setget setIsInput
export var state:int = State.UNDEF setget setState
export var isHint:bool = false setget setIsHint

var focused = false

func _ready():
	$GateIO.gate = self

func _draw():
	var vec0 = Vector2(radius0,max(radius0,radius1))
	var vec1 = Vector2(radius0*2+lineLength+radius1,vec0.y)
	set_size(Vector2(radius0*2+lineLength+radius1*2,vec0.y*2))
	var c:Color
	if focused:
		c = focusedColor
	else:
		match(state):
			State.HIGH:
				c = highColor
			State.LOW:
				c = lowColor
			State.UNDEF:
				c = color
			_:
				push_error("A404M:Undefined value of state")
	c.a = 0.8 if isHint else 1
	color.a = c.a
	draw_line(vec0,vec1,color,lineWidth)
	draw_circle(vec0,radius0,c)
	
	$ClickHelper.set_size(Vector2(radius0*2,radius0*2))


func setRadius0(val):
	radius0 = val
	update()

func setRadius1(val):
	radius1 = val
	$GateIO.radius = val
	update()

func setLineLength(val):
	lineLength = val
	update()
	
func setLineWidth(val):
	lineWidth = val
	update()

func setColor(val):
	color = val
	$GateIO.color = val
	update()

func setFocusedColor(val):
	focusedColor = val
	$GateIO.focusedColor = val
	update()

func setHighColor(val):
	highColor = val
	update()

func setLowColor(val):
	lowColor = val
	update()

func setIsInput(val):
	isInput = val
	$GateIO.isInput = val
	update()

func setState(val):
	state = val
	update()


func setIsHint(val):
	isHint = val
	$GateIO.visible = not isHint
	$ClickHelper.visible = not isHint
	mouse_filter = MOUSE_FILTER_IGNORE if isHint else MOUSE_FILTER_PASS
	state = State.UNDEF if isHint else State.LOW
	update()


func _on_GateIO_clicked(node):
	emit_signal("ioClicked",node,true,-1)


func _on_ClickHelper_gui_input(event:InputEvent):
	if event.is_action_pressed("mouse_left_click"):
		emit_signal("clicked")
		get_tree().get_root().set_input_as_handled()
		update()
	elif event.is_action_pressed("mouse_right_click"):
		get_tree().get_root().set_input_as_handled()
		emit_signal("rightClicked")


func _on_ClickHelper_mouse_entered():
	focused = true
	update()


func _on_ClickHelper_mouse_exited():
	focused = false
	update()


func serialize():
	return {
		"radius0":radius0,
		"radius1":radius1,
		"lineLength":lineLength,
		"lineWidth":lineWidth,
		"color":color,
		"focusedColor":focusedColor,
		"highColor":highColor,
		"lowColor":lowColor,
		"isInput":isInput,
		"state":state,
		"GateIO":$GateIO.serialize()
	}


func deserialize(dict):
	radius0 = dict.radius0
	radius1 = dict.radius1
	lineLength = dict.lineLength
	lineWidth = dict.lineWidth
	color = dict.color
	focusedColor = dict.focusedColor
	highColor = dict.highColor
	lowColor = dict.lowColor
	isInput = dict.isInput
	state = dict.state
	$GateIO.deserialize(dict.GateIO)
	update()
	return self
