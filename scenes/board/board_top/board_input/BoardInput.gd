extends Control

class_name BoardInput

signal ioClicked(node,isInput,i)
signal delete

func _ready():
	$BoardIO.state = State.LOW

func _on_BoardIO_ioClicked(node, isInput, i):
	emit_signal("ioClicked",node,true,i)


func serialize():
	return {
		"BoardIO":$BoardIO.serialize()
	}

func deserialize(dict):
	$BoardIO.deserialize(dict.BoardIO)
	update()
	return self


func _on_BoardIO_rightClicked():
	$RightClickMenu.popup()
	var pos = get_global_mouse_position()
	var p0 = $RightClickMenu.rect_size
	var win = OS.window_size
	if pos.x + p0.x >= win.x:
		pos.x = win.x-p0.x
	
	if pos.y + p0.y >= win.y:
		pos.y = win.y-p0.y
	
	$RightClickMenu.set_global_position(pos)
	#if $RightClickMenu.
	#get_parent().get_parent().removeInput(self)


func getState():
	return $BoardIO.state


func getWire():
	return $BoardIO/GateIO.getWire()


func _on_BoardIO_clicked():
	$BoardIO.setState(State.HIGH if $BoardIO.state == State.LOW else State.LOW)


func _on_RightClickMenu_id_pressed(id:int):
	match(id):
		0:#delete
			emit_signal("delete")
		1:#high
			$BoardIO.setState(State.HIGH)
		2:#low
			$BoardIO.setState(State.LOW)


func setIsHint(val:bool):
	$BoardIO.isHint = val


func getIsHint():
	return $BoardIO.isHint
