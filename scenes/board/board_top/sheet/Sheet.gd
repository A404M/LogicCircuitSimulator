tool

extends Control

signal clicked(globalPos)

const Gate = preload("res://scenes/gate/Gate.tscn")
const SceneHelper = preload("res://scripts/SceneHelper.gd")
const Wire = preload("res://scenes/wire/Wire.tscn")

export var background:Color = Color("333333") setget setBackground

func _draw():
	$Background.color = background


func setBackground(val):
	background = val
	update()


func addGate(gate:Gate):
	gate.connect("ioClicked",get_parent(),"_on_IO_ioClicked")
	#gate.connect("ioClicked",self,"_on_Gate_ioClicked")
	gate.connect("clicked",get_parent(),"_on_Gate_clicked")
	gate.connect("delete",self,"_on_Gate_delete",[gate])
	$GateHolder.add_child(gate)
	gate.set_owner($GateHolder)
	gate.name = str($GateHolder.get_child_count())


func removeGate(gate:Gate):
	SceneHelper.removeNumericNamedChild($GateHolder,gate)


func clear():
	SceneHelper.freeChilds($GateHolder)
	SceneHelper.freeChilds($WireHolder)

func getWires():
	return $WireHolder.get_children()

func getGates():
	return $GateHolder.get_children()


func serialize():
	var gates = []
	var wires = []
	for gate in $GateHolder.get_children():
		gates.append(gate.serialize())
	for wire in $WireHolder.get_children():
		wires.append(wire.serialize())
	return {
		"background":background,
		"gates":gates,
		"wires":wires
	}


func deserialize(dict):
	SceneHelper.freeChilds($GateHolder)
	SceneHelper.freeChilds($WireHolder)
	background = dict.background
	for gate in dict.gates:
		addGate(Gate.instance().deserialize(gate))
	#var temp = Control.new()
	#add_child(temp)
	for wire in dict.wires:
		var w = Wire.instance()
		w.deserialize(wire,$WireHolder)# TODO:
		#w.first.wireTo(w.second,$WireHolder).deserialize(wire)
		#temp.remove_child(w)
		#w.queue_free()
	update()
	return self


func _on_Gate_delete(gate:Gate):
	SceneHelper.removeNumericNamedChild($GateHolder,gate)


func _on_GateHolder_gui_input(event:InputEvent):
	if event.is_action_pressed("mouse_left_click"):
		emit_signal("clicked",(event as InputEventMouse).global_position)
		get_tree().get_root().set_input_as_handled()
