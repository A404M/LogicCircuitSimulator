extends Control

var MainMenu = load("res://scenes/main_menu/MainMenu.tscn")
const SceneHelper = preload("res://scripts/SceneHelper.gd")
const Gate = preload("res://scenes/gate/Gate.tscn")
const Pair = preload("res://scripts/Pair.gd")
#var Sakht = load("res://scripts/Sakht.gd")
const VerilogHelper = preload("res://scripts/VerilogHelper.gd")
#const Verilog = preload("res://native_script/Verilog.gdns")
const DBHelper = preload("res://scripts/DBHelper.gd")
const SettingsGD = preload("res://scenes/settings/Settings.gd")

var assets = [
	Gate.instance().setAll("57BDD3","NAND",2,1),
	Gate.instance().setAll("8998af","BUF",1,1),
	Gate.instance().setAll("d96868","BUFIF1",2,1),
	]
#var verilog = Verilog.new()
var db := DBHelper.new()


func _ready():
	$Timer.wait_time = SettingsGD.getEvalWaitTime()
	initAssets()


func _on_ButtomBar_newBoard():
	loadNewBoard()


func _on_ButtomBar_save():
	saveBoardTop()


func _on_BottomBar_loadBoard():
	showLoadList()


func _on_ButtomBar_mainMenu():
	#saveBoardTop()
	gotoMainMenu()

func _enter_tree():
	if db == null:
		db = DBHelper.new()


func _exit_tree():
	db.close()
	db = null


func serialize():
	return {
		"BoardTop":$BoardTop.serialize()
	}


func deserialize(dict):
	$BoardTop.deserialize(dict.BoardTop)
	update()
	return self


func saveBoardTop()->void:
	if $BoardTop.boardName != "":
		db.insertIfNotExists($BoardTop.boardName.to_lower(),$BoardTop.serialize(),VerilogHelper.convertToVerilog($BoardTop))
	initAssets()


func showLoadList():
	$SaveScreen.hide()
	$LoadList.items = db.boardNames()	
	$LoadList.show()
	return self


func loadBoardTop(boardName:String)->void:
	var dict = db.selectBoard(boardName)
	$BottomBar/Container/NameLabel.text = "    "+dict.data.boardName
	$BoardTop.deserialize(dict.data)
	lastCode = dict.code
	initAssets()


func loadNewBoard():
	$BoardTop.clear()
	$Timer.stop()
	$SaveScreen.show()
	initAssets()


func deleteBoardTop(boardName:String)->void:
	db.deleteBoard(boardName)
	initAssets()


func initAssets():
	$BottomBar.clearAssets()
	assets.resize(3)
	var boards = db.boardNames()
	for boardName in boards:
		assets.push_back(db.getBoardAsGate(boardName))
	
	for asset in assets:
		$BottomBar.addItemToAsset(asset.gateName)


func gotoMainMenu():
	SceneHelper.changeScene(self,MainMenu.instance())	


func _on_ButtomBar_assetPressed(id):
	var gate = assets[id].duplicate()
	$BoardTop/Sheet.addGate(gate)

var lastCode := ""
func run():
	lastCode = VerilogHelper.run($BoardTop,$Verilog,lastCode)
	if lastCode == "":
		compileAllModules()


func compileAllModules():
	$Verilog.clearModules()
	var boardNames = db.boardNames()
	
	for boardName in boardNames:
		$Verilog.compile(
			db.selectBoard(boardName).code
		)


func _on_Timer_timeout():
	#return
	run()


func _on_SaveScreen_nameEntered(nameEntered):
	#(nameEntered)
	if nameEntered in db.boardNames():
		$SaveScreen.setError("Name exists in database")
		return
	lastCode = ""
	$Verilog.clearModules()
	$BoardTop.boardName = nameEntered
	$BottomBar/Container/NameLabel.text = "    "+nameEntered
	$SaveScreen.hide()
	$Timer.start()


func _on_SaveScreen_canceled():
	if $BoardTop.boardName == "":
		gotoMainMenu()
	else:
		$SaveScreen.hide()
		$Timer.start()


func _on_LoadList_canceled():
	$LoadList.hide()
	if $BoardTop.boardName == "":
		gotoMainMenu()
	$Timer.start()


func _on_LoadList_loadItem(itemName:String):
	loadBoardTop(itemName)
	$LoadList.hide()


func _on_LoadList_deleteItem(itemName:String):
	deleteBoardTop(itemName)
	$LoadList.hide()
