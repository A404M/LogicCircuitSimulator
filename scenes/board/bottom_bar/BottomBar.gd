tool

extends Control

signal newBoard
signal save
signal loadBoard
signal mainMenu
signal assetPressed(id)

export var background:Color = Color('383838')

func _ready():
	$Container/Menu.get_popup().connect("id_pressed",self,"_on_Menu_popup_id_pressed")
	$Container/Assets.get_popup().connect("id_pressed",self,"_on_Asset_popup_id_pressed")


func _draw():
	$Background.color = background

func _on_Menu_popup_id_pressed(id):
	match(id):
		0:#new
			emit_signal("newBoard")
		1:#save
			emit_signal("save")
		2:#load
			emit_signal("loadBoard")
		3:#mainMenu
			emit_signal("mainMenu")
		_:
			push_error("bad id %s"%id)

func _on_Asset_popup_id_pressed(id):
	emit_signal("assetPressed",id)

func addItemToAsset(itemName:String):
	$Container/Assets.get_popup().add_item(itemName)

func clearAssets():
	$Container/Assets.get_popup().clear()
