tool

extends Control

signal loadItem(itemName)
signal deleteItem(itemName)
signal canceled

var items := [] setget setItems
var selected:Button = null


func setItems(newItems):
	clearItems()
	for item in newItems:
		addItem(item)


func clearItems():
	items.clear()
	SceneHelper.freeChilds($PanelContainer/ScrollContainer/VBoxContainer)


func addItem(item:String):
	var child = Button.new()
	child.text = item
	child.connect("pressed",self,"_on_Item_pressed",[child])
	child.rect_min_size.x = $PanelContainer/ScrollContainer.rect_min_size.x
	items.push_back(child)
	$PanelContainer/ScrollContainer/VBoxContainer.add_child(child)
	child.set_owner($PanelContainer/ScrollContainer/VBoxContainer)


func removeItem(item:String):
	var index = findItem(item)
	if index == -1:
		return
	var child:Button = items[index]
	items.remove(index)
	$PanelContainer/ScrollContainer/VBoxContainer.remove_child(child)
	child.queue_free()


func findItem(item:String):
	for i in range(items.size()):
		if items[i] == item:
			return i
	return -1


func _on_Item_pressed(item:Button):
	if selected != null:
		selected.disabled = false
	item.disabled = true
	selected = item


func _input(event:InputEvent):
	if event.is_action_pressed("cancel"):
		#get_tree().get_root().set_input_as_handled()
		selected = null
		emit_signal("canceled")


func _on_Load_pressed():
	if selected != null:
		emit_signal("loadItem",selected.text)
	selected = null


func _on_Delete_pressed():
	if selected != null:
		emit_signal("deleteItem",selected.text)
	selected = null
