extends Control

var BoardScene = load("res://scenes/board/Board.tscn")
const SceneHelper = preload("res://scripts/SceneHelper.gd")
const About = preload("res://scenes/about/About.tscn")
const Settings = preload("res://scenes/settings/Settings.tscn")

func _on_New_pressed():
	SceneHelper.changeScene(self,BoardScene.instance())


func _on_Load_pressed():
	SceneHelper.changeScene(self,BoardScene.instance().showLoadList())


func _on_Settings_pressed():
	SceneHelper.changeScene(self,Settings.instance())


func _on_About_pressed():
	SceneHelper.changeScene(self,About.instance())


func _on_Exit_pressed():
	get_tree().quit()
