extends Control


func _ready():
	# set minimum size of window to 400x400
	OS.min_window_size = Vector2(400, 400)
	#randomize()

func _input(event:InputEvent):
	# full screen mode
	if event.is_action_pressed("full_screen_button"):
		get_tree().get_root().set_input_as_handled()
		OS.window_fullscreen = !OS.window_fullscreen
	# show or hide fps in corner
	elif event.is_action_pressed("show_fps"):
		$FPSLabel.visible = not $FPSLabel.visible
		get_tree().get_root().set_input_as_handled()

func _process(delta):
	# show fps in label
	$FPSLabel.text = "FPS: "+String(Engine.get_frames_per_second())
