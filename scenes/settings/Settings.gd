extends Control

var MainMenu = load("res://scenes/main_menu/MainMenu.tscn")

const PATH := "user://settings.ini"


func _ready():
	$RowHolder/EvalWaitTimeHolder/EvalWaitTimeLineEdit.text = String(getEvalWaitTime())


func _draw():
	pass
	#$EvalWaitTimeHolder.center

func _input(event:InputEvent):
	if event.is_action_pressed("cancel"):
		#get_tree().get_root().set_input_as_handled()
		if not $RowHolder/EvalWaitTimeHolder/EvalWaitTimeLineEdit.text.is_valid_float():
			return
		setEvalWaitTime(float($RowHolder/EvalWaitTimeHolder/EvalWaitTimeLineEdit.text))
		SceneHelper.changeScene(self,MainMenu.instance())


static func loadConf()->ConfigFile:
	var conf = ConfigFile.new()
	
	var error = conf.load(PATH)
	if error != OK:
		conf = null
		return initConf()
	return conf


static func initConf()->ConfigFile:
	var conf = ConfigFile.new()
	conf.set_value("board","eval_wait_time",0.05)
	conf.save(PATH)
	return conf


static func getEvalWaitTime()->float:
	var conf = loadConf()
	return conf.get_value("board","eval_wait_time")


static func setEvalWaitTime(time:float)->void:
	var conf = loadConf()
	conf.set_value("board","eval_wait_time",time)
	conf.save(PATH)


func _on_EvalWaitTimeLineEdit_text_changed(new_text:String):
	if new_text.is_valid_float():
		$RowHolder/EvalWaitTimeHolder/Error.text = ""
	else:
		$RowHolder/EvalWaitTimeHolder/Error.text = "Value is not a floating point number."
