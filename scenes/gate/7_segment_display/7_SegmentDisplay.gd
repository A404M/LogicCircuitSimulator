tool

extends Control

var inputs := []

func _ready():
	for i in range(9):
		inputs.push_back(State.LOW)

func _draw():
	for i in range(inputs.size()):
		$Segments.get_node("S"+String(i)).state = inputs[i]
