tool

class_name Segment

extends Control

export var highColor := Color("d62d2d") setget setHighColor
export var lowColor := Color("242327") setget setLowColor
export var state := State.LOW setget setState

func _draw():
	$Shape.color = highColor if state == State.HIGH else lowColor


func setHighColor(val:Color):
	highColor = val
	update()


func setLowColor(val:Color):
	lowColor = val
	update()


func setState(val):
	isHigh = val
	update()
