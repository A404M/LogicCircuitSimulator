tool

class_name GateIO

extends Control

signal clicked(node)

const Wire = preload("res://scenes/wire/Wire.tscn")

export var color:Color = Color('232323')
export var focusedColor:Color = Color('888888')
export var radius:float = 8

var gate
var wire:Wire = null
var isInput = true

var focused = false

func _draw():
	draw_circle(Vector2(radius,radius),radius,focusedColor if focused else color)
	set_size(Vector2(radius*2,radius*2),true)
	updateWires()


func _exit_tree():
	if wire != null:
		wire.remove()


func _on_GateIO_mouse_entered():
	focused = true
	update()


func _on_GateIO_mouse_exited():
	focused = false
	update()


func _on_GateIO_gui_input(event):
	if event.is_action_pressed("mouse_left_click"):
		get_tree().get_root().set_input_as_handled()
		emit_signal("clicked",self)


func updateWires():
	#for wire in wires:
	if wire != null:
		wire.update()


func getWire()->Wire:
	return wire


func setWire(w:Wire):
	wire = w


func wireTo(node:GateIO,wireHolder:Control,hintWire:Wire):
	if self.wire == null:
		if node.wire == null:
			wire = Wire.instance()
			wireHolder.add_child(wire)
			wire.name = str(wireHolder.get_child_count())
			wire.set_owner(wireHolder)
			wire.addIO(self)
			wire.addIO(node,hintWire)
			node.wire = wire
			wire.update()
		else:
			node.wireTo(self,wireHolder,hintWire.reverse())
	else:
		if node.wire == null:
			self.wire.addIO(node,hintWire,self.getCenterNode())
			node.wire = self.wire
			wire.update()
		else:
			self.wire.connectToWire(node.wire,self.getCenterNode(),hintWire)


func serialize():
	return {
		"color":color,
		"focusedColor":focusedColor,
		"radius":radius,
		"gate":gate.get_path(),
		"filename":filename
	}


func deserialize(dict):
	color = dict.color
	focusedColor = dict.focusedColor
	radius = dict.radius
	gate = get_node(dict.gate)
	update()
	return self


func getCenterNode()->Control:
	return $Center as Control
