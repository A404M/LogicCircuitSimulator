tool

class_name Gate

extends Control

signal ioClicked(node,isInput,i)
signal clicked
#signal rightClicked(node)
signal delete

const GateIO = preload("res://scenes/gate/gate_io/GateIO.tscn")
const SceneHelper = preload("res://scripts/SceneHelper.gd")
const Pair = preload("res://scripts/Pair.gd")
const State = preload("res://scripts/State.gd")

export var color:Color = Color('83A3D2') setget setColor
export var gateName:String = "gate" setget setGateName
export var inputNum:int = 0 setget setInputNum
export var outputNum:int = 0 setget setOutputNum

var ioWH:int = 16
var ioSep:int = 20




func _ready():
	#ready()
	ioWH = GateIO.instance().radius*2
	ioSep = ioWH+4
	$Inputs.add_constant_override("separation", ioSep)
	$Outputs.add_constant_override("separation", ioSep)

func _draw():
	doDrawing()

func doDrawing():
	$ColorRect/Rect.color = color
	$ColorRect/GateName.text = gateName
	
	drawRect()
	
	resetInputGates()
	resetOutputGates()

func drawRect():
	var label = $ColorRect/GateName
	var x = label.rect_size.x+20
	var y = max(
		max(
			max(inputNum,1),outputNum)*ioSep,
			label.rect_size.y
		)+4
	set_size(Vector2(x,y))
	
	#$ClickHelper.set_size(Vector2(x,y))


func _on_GateName_draw():
	update()

func resetInputGates():
	resetGates($Inputs,inputNum,true)


func resetOutputGates():
	resetGates($Outputs,outputNum,false)


func resetGates(node:Node,num:int,isInput):
	var count = node.get_child_count()
	if num == count:
		return
	elif num < count:
		for _i in range(num,count,-1):
			node.remove_child(node.get_child(node.get_child_count()-1))
	else:
		for _i in range(count,num):
			var io = GateIO.instance()
			io.connect("clicked",self,"_on_GateIO_clicked")
			io.gate = self
			io.isInput = isInput
			node.add_child(io)
			io.name = str(node.get_child_count())


func setColor(val):
	color = val
	update()

func setGateName(val):
	gateName = val
	update()

func setInputNum(val):
	inputNum = val
	update()

func setOutputNum(val):
	outputNum = val
	update()

func getInputs():
	return $Inputs.get_children()

func getOutputs():
	return $Outputs.get_children()

func setAll(color_,gateName_,inputNum_,outputNum_):
	self.color = color_
	self.gateName = gateName_
	self.inputNum = inputNum_
	self.outputNum = outputNum_
	return self


func serialize():
	var inputs = []
	var outputs = []
	for input in $Inputs.get_children():
		inputs.append(input.serialize())
	for output in $Outputs.get_children():
		outputs.append(output.serialize())
	return {
		"color":color,
		"gateName":gateName,
		"inputNum":inputNum,
		"outputNum":outputNum,
		"ioWH":ioWH,
		"ioSep":ioSep,
		"inputs":inputs,
		"outputs":outputs,
		"posX":rect_global_position.x,
		"posY":rect_global_position.y
	}


func deserialize(dict):
	color = dict.color
	gateName = dict.gateName
	inputNum = dict.inputNum
	outputNum = dict.outputNum
	ioWH = dict.ioWH
	ioSep = dict.ioSep
	resetInputGates()
	resetOutputGates()
	set_global_position(Vector2(dict.posX,dict.posY))
	update()
	return self



	
func _on_GateIO_clicked(node):
	for i in range($Inputs.get_child_count()):
		if node == $Inputs.get_child(i):
			emit_signal("ioClicked",node,true,i)
			return
	
	for i in range($Outputs.get_child_count()):
		if node == $Outputs.get_child(i):
			emit_signal("ioClicked",node,false,i)
			return
	#status = Status.RELEASED


func _on_RightClickMenu_id_pressed(id):
	match(id):
		0:#delete
			emit_signal("delete")

func _input(event:InputEvent):
	if event.is_action_pressed("cancel"):
		#get_tree().get_root().set_input_as_handled()
		dragging = false





#dragging

var dragging = false

#var tsize = Vector2()

var offset = Vector2.ZERO

#func ready():
#	tsize = Vector2(get_rect().size.x,get_rect().size.y)
#	rect_position = get_rect().size / 2


func _process(_delta):
	if dragging:
		var newPos = get_global_mouse_position() + offset
		var mp = get_parent().get_local_mouse_position()
		var pp = get_parent().rect_size
		var off = get_local_mouse_position()
		var s = rect_size
		var endOff = s-off
		if (mp-off).x < 0:
			newPos.x = 1
			dragging = false
		elif (mp+endOff).x > pp.x:
			newPos.x = pp.x-s.x-1
			dragging = false
		if (mp-off).y < 0:
			newPos.y = 1
			dragging = false
		elif (mp+endOff).y > pp.y:
			newPos.y = pp.y-s.y-1
			dragging = false
		
		rect_position = newPos
		
		for input in $Inputs.get_children():
			input.updateWires()
		for output in $Outputs.get_children():
			output.updateWires()


func _on_ClickHelper_gui_input(event:InputEvent):
	if event.is_action_pressed("mouse_left_click"):
		emit_signal("clicked")
		get_tree().get_root().set_input_as_handled()
		#if not dragging:
		#	offset = rect_position - event.global_position
		#dragging = not dragging
	elif event.is_action_pressed("mouse_right_click"):
		get_tree().get_root().set_input_as_handled()
		dragging = false
		$RightClickMenu.popup()
		var pos = get_global_mouse_position()
		var p0 = $RightClickMenu.rect_size
		var win = OS.window_size
		if pos.x + p0.x >= win.x:
			pos.x = win.x-p0.x
		
		if pos.y + p0.y >= win.y:
			pos.y = win.y-p0.y
		$RightClickMenu.set_global_position(pos)
		#emit_signal("rightClicked",self)
	
	if event is InputEventMouseButton:
		dragging = event.is_action_pressed("mouse_left_click")
		offset = rect_position - event.global_position
